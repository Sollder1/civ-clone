package de.sollder1.civklon.cities;

import de.sollder1.civklon.EndTurnAware;

public interface City extends EndTurnAware {

    String getName();

    float getPopulation();

    float getBirthrate();

}
