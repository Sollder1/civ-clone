package de.sollder1.civklon.state;

import de.sollder1.civklon.constant.GlobalConstants;
import de.sollder1.civklon.utils.TileCoord;

import java.util.Comparator;

public final class FoodYieldCalculator {

    public static int hunterGatherer(boolean dryRun) {

        var currentPosition = state().getEarlyTribeTile();

        if (currentPosition.isEmpty()) {
            return 0;
        }

        GameState gameState = GameState.getInstance();
        var tiles = gameState.findInTileRadius(currentPosition.get().pos(), gameState.getYieldRadius(),
                coord -> GlobalConstants.TECH_GRAPH.getPassableTerrains().contains(coord.tile().getTerrain().getMeta().id()));
        tiles.sort(Comparator.comparing(Object::hashCode));

        int yield = 0;
        int availablePopulation = (int) state().getEntity().getPopulation();

        for (TileCoord tile : tiles) {
            if (availablePopulation == 0) {
                break;
            }
            int maxFieldYieldPerPop = GlobalConstants.TECH_GRAPH.getFoodYield(tile.tile().getTerrainId());
            if (maxFieldYieldPerPop == 0) {
                continue;
            }

            int tileResourceCap = tile.tile().getResources();
            int actuallyExpandedPops = Math.min(availablePopulation, (tileResourceCap - ((tileResourceCap % maxFieldYieldPerPop))) / maxFieldYieldPerPop);
            int tileYield = maxFieldYieldPerPop * actuallyExpandedPops;
            yield += tileYield;
            availablePopulation -= actuallyExpandedPops;
            if (!dryRun) {
                tile.tile().harvest(tileYield);
            }
        }

        return yield;
    }

    private static GameState state() {
        return GameState.getInstance();
    }

}
