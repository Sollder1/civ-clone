package de.sollder1.civklon.state;

import de.sollder1.civklon.EndTurnAware;
import de.sollder1.civklon.constant.GlobalConstants;
import de.sollder1.civklon.entities.EarlyTribe;
import de.sollder1.civklon.entities.Entity;
import de.sollder1.civklon.technology.Event;
import de.sollder1.civklon.technology.TechGraph;
import de.sollder1.civklon.tiles.Tile;
import de.sollder1.civklon.tiles.TileTerrain;
import de.sollder1.civklon.utils.Index2D;
import de.sollder1.civklon.utils.TileCoord;
import lombok.Data;

import java.util.*;
import java.util.function.Consumer;
import java.util.function.Predicate;

@Data
public class GameState {

    private static GameState INSTANCE;

    public static GameState getInstance() {
        return INSTANCE;
    }

    public static void init(Tile[][] initialTiles) {
        INSTANCE = new GameState();
        INSTANCE.tiles = initialTiles;

        final List<Tile> grasTiles = new ArrayList<>();

        for (Tile[] initialTile : initialTiles) {
            for (int j = 0; j < initialTiles.length; j++) {
                Tile tile = initialTile[j];
                if (TileTerrain.GRASS_LAND_ID.equals(tile.getTerrain().getMeta().id())) {
                    grasTiles.add(tile);
                }
            }
        }
        EarlyTribe entity = new EarlyTribe();
        grasTiles.get(GlobalConstants.SECURE_RANDOM.nextInt(grasTiles.size())).setEntity(entity);
        INSTANCE.entity = entity;
        INSTANCE.passableTerrains = GlobalConstants.TECH_GRAPH.getPassableTerrains();
    }

    private int round = 1;
    private boolean gameOver;

    private int food = 2;
    private EarlyTribe entity;
    private float technologyYieldPerPop = 0.05f;

    private TechGraph.Node currentResearch;
    private float currentResearchProgress;

    private int yieldRadius = 0;

    private Tile[][] tiles;
    private Set<String> passableTerrains = new HashSet<>();
    private Set<String> currentTechnologies = new HashSet<>(List.of("basically_apes"));



    public int getHeight() {
        return tiles.length;
    }

    public int getWidth() {
        return tiles[0].length;
    }


    public Optional<TileCoord> getEarlyTribeTile() {
        return getEntityTile(entity);
    }

    public TileCoord getEarlyTribeTileOrThrow() {
        return getEntityTile(entity).orElseThrow(() -> new IllegalStateException("No early tribe found"));
    }

    private Optional<TileCoord> getEntityTile(Entity toLookup) {
        for (int i = 0; i < tiles.length; i++) {
            for (int j = 0; j < tiles[i].length; j++) {
                Tile tile = tiles[i][j];
                if (tile.getEntity() == toLookup) {
                    return Optional.of(new TileCoord(tile, new Index2D(i, j)));
                }
            }
        }
        return Optional.empty();
    }

    public List<TileCoord> findInTileRadius(Index2D origin, int radius, Predicate<TileCoord> predicate) {
        List<TileCoord> coords = new ArrayList<>();
        for (int iDelta = radius * -1; iDelta <= radius; iDelta++) {
            for (int jDelta = radius * -1; jDelta <= radius; jDelta++) {
                Index2D pos = new Index2D(origin.i() + iDelta, origin.j() + jDelta);
                if (pos.i() > tiles.length || pos.j() > tiles[0].length) {
                    continue;
                }

                Tile tile = tiles[pos.i()][pos.j()];
                TileCoord tileCoord = new TileCoord(tile, pos);
                if (predicate.test(tileCoord)) {
                    coords.add(tileCoord);
                }
            }
        }
        return coords;
    }

    public void forEachTile(Consumer<Tile> runner) {
        for (int i = 0; i < tiles.length; i++) {
            for (int j = 0; j < tiles[i].length; j++) {
                Tile tile = tiles[i][j];
                runner.accept(tile);
            }
        }
    }

    public void nextRound() {
        updateFood();
        runEndOfTurn();
        handleResearch();
        round++;
    }

    private void handleResearch() {
        if (currentResearch == null) {
            return;
        }

        currentResearchProgress += technologyYieldPerPop * (int) entity.getPopulation();
        if (currentResearchProgress >= currentResearch.researchRequired()) {
            currentTechnologies.add(currentResearch.id());
            if (currentResearch.triggers() != null) {
                try {
                    Object trigger = Class.forName(currentResearch.triggers()).getDeclaredConstructor().newInstance();
                    ((Event) trigger).trigger();
                } catch (ReflectiveOperationException e) {
                    throw new RuntimeException(e);
                }
            }
            currentResearch = null;
            currentResearchProgress = 0;

        }

    }

    private void runEndOfTurn() {
        forEachTile(tile -> {
            tile.endTurn();
            if (tile.getEntity() instanceof EndTurnAware eta) {
                eta.endTurn();
            }
        });
    }


    private void updateFood() {
        food -= getProjectedFoodConsumption();
        food += FoodYieldCalculator.hunterGatherer(false);
        if (food < 0) {
            food = 0;
        }
    }

    public int calculateFoodYield() {
        return FoodYieldCalculator.hunterGatherer(true);
    }

    public int getProjectedFoodConsumption() {
        return (int) entity.getPopulation();
    }

    public void setCurrentResearch(TechGraph.Node currentResearch) {
        this.currentResearchProgress = 0.f;
        this.currentResearch = currentResearch;
    }
}
