package de.sollder1.civklon.entities;

import com.badlogic.gdx.graphics.Texture;
import de.sollder1.civklon.EndTurnAware;
import de.sollder1.civklon.utils.TileCoord;

public interface Entity extends EndTurnAware {

    Texture getTexture();

    void onMove();

    boolean canMove(TileCoord target);
}
