package de.sollder1.civklon.entities;

import com.badlogic.gdx.graphics.Texture;
import de.sollder1.civklon.state.GameState;
import de.sollder1.civklon.cities.City;
import de.sollder1.civklon.constant.GlobalConstants;
import de.sollder1.civklon.utils.TileCoord;
import lombok.Data;

@Data
public class EarlyTribe implements Entity, City {

    private static Texture texture;
    private int lastMove;
    private float population = 1;

    public EarlyTribe() {
        texture = new Texture("textures/tiles/entities/early_tribe.png");
    }


    @Override
    public Texture getTexture() {
        return texture;
    }

    @Override
    public void onMove() {
        lastMove = GameState.getInstance().getRound();
    }

    @Override
    public boolean canMove(TileCoord target) {
        TileCoord tileCoord = GameState.getInstance().getEarlyTribeTile().orElseThrow();
        int iDistance = Math.abs(target.pos().i() - tileCoord.pos().i());
        int jDistance = Math.abs(target.pos().j() - tileCoord.pos().j());

        if (iDistance > 1 || jDistance > 1) {
            return false;
        }

        if (!GameState.getInstance().getPassableTerrains().contains(target.tile().getTerrainId())) {
            return false;
        }

        return GameState.getInstance().getRound() != lastMove;
    }


    @Override
    public String getName() {
        return "-";
    }

    @Override
    public float getPopulation() {
        return population;
    }

    @Override
    public float getBirthrate() {
        return 0.02f;
    }

    @Override
    public void endTurn() {
        population += calculatePopDevelopment(true);
    }

    public float calculatePopDevelopment() {
        return calculatePopDevelopment(false);
    }


    private float calculatePopDevelopment(boolean includeRandom) {
        float newPops = getBirthrate() * getPopulation();
        GameState state = GameState.getInstance();
        if (state.getFood() <= 0 && (state.calculateFoodYield() - state.getProjectedFoodConsumption() < 0)) {
            newPops = population * -0.2f;
        }
        return Math.round(newPops * 100) / 100f;
    }

}
