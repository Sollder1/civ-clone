package de.sollder1.civklon.utils;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.fasterxml.jackson.dataformat.yaml.YAMLMapper;

import java.io.IOException;
import java.io.InputStream;

public final class JsonUtils {

    private static final JsonMapper JSON_MAPPER = new JsonMapper();
    private static final YAMLMapper YML_MAPPER = new YAMLMapper();




    public static  <T> T load(TypeReference<T> reference, InputStream data) {
        try {
            return JSON_MAPPER.readValue(data, reference);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static  <T> T loadYml(TypeReference<T> reference, InputStream data) {
        try {
            return YML_MAPPER.readValue(data, reference);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
