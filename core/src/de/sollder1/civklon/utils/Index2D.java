package de.sollder1.civklon.utils;

import com.badlogic.gdx.math.Vector2;

public record Index2D(int i, int j) {

    public Vector2 toVector(int normalTileSize) {
        return new Vector2(j * normalTileSize, i * normalTileSize);
    }
}
