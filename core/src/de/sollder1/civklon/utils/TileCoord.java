package de.sollder1.civklon.utils;

import de.sollder1.civklon.tiles.Tile;

public record TileCoord(Tile tile, Index2D pos) {

}
