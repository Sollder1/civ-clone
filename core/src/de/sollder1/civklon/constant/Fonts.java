package de.sollder1.civklon.constant;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import lombok.Getter;

@Getter
public class Fonts {

    @Getter
    private static Fonts instance;


    public static void init() {
        FileHandle fontFile = Gdx.files.internal("fonts/Roboto-Regular.ttf");
        instance = new Fonts();
        instance.normalFont = buildFont(fontFile, 24);
        instance.smallFont = buildFont(fontFile, 16);
    }

    private static BitmapFont buildFont(FileHandle fontFile, int size) {
        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(fontFile);
        try {
            generator.scaleForPixelHeight(size);
            FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
            parameter.size = size;
            parameter.minFilter = Texture.TextureFilter.Linear;
            parameter.magFilter = Texture.TextureFilter.Linear;
            return generator.generateFont(parameter);
        } finally {
            generator.dispose();
        }

    }

    public static void cleanup() {
        instance.normalFont.dispose();
        instance.smallFont.dispose();
        instance = null;
    }

    private BitmapFont normalFont;
    private BitmapFont smallFont;


    private Fonts() {

    }


}
