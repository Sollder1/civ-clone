package de.sollder1.civklon.constant;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.fasterxml.jackson.core.type.TypeReference;
import de.sollder1.civklon.technology.TechGraph;
import de.sollder1.civklon.tiles.TileDecorator;
import de.sollder1.civklon.tiles.TileTerrain;
import de.sollder1.civklon.utils.JsonUtils;

import java.io.IOException;
import java.io.InputStream;
import java.security.SecureRandom;
import java.util.List;

public final class GlobalConstants {

    public static final List<TileTerrain> TILE_TERRAINS = loadTileTerrains();
    public static final List<TileDecorator> TILE_DECORATORS = loadTileDecorators();
    public static final TechGraph TECH_GRAPH = loadTechGraph();

    private static TechGraph loadTechGraph() {
        TypeReference<List<TechGraph.Phase>> reference = new TypeReference<>() {
        };
        FileHandle fileHandle = Gdx.files.internal("technologies.yml");
        try (InputStream resourceAsStream = fileHandle.read()) {
            var phases = JsonUtils.loadYml(reference, resourceAsStream);
            return new TechGraph(phases);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static final SecureRandom SECURE_RANDOM = new SecureRandom();

    private static List<TileTerrain> loadTileTerrains() {
        TypeReference<List<TileTerrain.JsonModel>> reference = new TypeReference<>() {
        };
        FileHandle fileHandle = Gdx.files.internal("tile_terrain.json");
        try (InputStream resourceAsStream = fileHandle.read()) {
            return JsonUtils.load(reference, resourceAsStream).stream()
                    .map(TileTerrain::new)
                    .toList();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static List<TileDecorator> loadTileDecorators() {
        TypeReference<List<TileDecorator.JsonModel>> reference = new TypeReference<>() {
        };
        FileHandle fileHandle = Gdx.files.internal("tile_decorators.json");
        try (InputStream resourceAsStream = fileHandle.read()) {
            return JsonUtils.load(reference, resourceAsStream).stream()
                    .map(TileDecorator::new)
                    .toList();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private GlobalConstants() {

    }


}
