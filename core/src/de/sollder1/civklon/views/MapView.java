package de.sollder1.civklon.views;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import de.sollder1.civklon.GameRenderer;
import de.sollder1.civklon.constant.Fonts;
import de.sollder1.civklon.state.GameState;
import de.sollder1.civklon.entities.Entity;
import de.sollder1.civklon.tiles.Tile;
import de.sollder1.civklon.utils.Index2D;
import de.sollder1.civklon.utils.TileCoord;

import java.util.Map;

public class MapView implements View {


    private OrthographicCamera mapCamera;
    private Viewport mapViewPort;
    private SpriteBatch mapBatch;
    private ShapeRenderer mapShapeRenderer;

    private Viewport hudViewport;
    private SpriteBatch hudBatch;
    private ShapeRenderer hudShapeRenderer;

    private Tile selected;

    public MapView() {

        mapCamera = new OrthographicCamera();
        mapCamera.setToOrtho(false, GameRenderer.V_WIDTH, GameRenderer.V_HEIGHT);
        mapViewPort = new FitViewport(GameRenderer.V_WIDTH, GameRenderer.V_HEIGHT, mapCamera);
        mapViewPort.apply(true);
        mapCamera.zoom = 1f;
        mapCamera.position.set(state().getEarlyTribeTile().get().pos().toVector(Tile.NORMAL_TILE_SIZE), 0);
        mapCamera.rotate(0);
        mapCamera.update();

        mapBatch = new SpriteBatch();
        mapBatch.setProjectionMatrix(mapCamera.combined);
        mapShapeRenderer = new ShapeRenderer();
        mapShapeRenderer.setAutoShapeType(true);
        mapShapeRenderer.setProjectionMatrix(mapCamera.combined);


        OrthographicCamera hudCamera = new OrthographicCamera();
        hudCamera.setToOrtho(false, GameRenderer.V_WIDTH, GameRenderer.V_HEIGHT);
        hudViewport = new FitViewport(GameRenderer.V_WIDTH, GameRenderer.V_HEIGHT, hudCamera);
        hudBatch = new SpriteBatch();
        hudBatch.setProjectionMatrix(hudViewport.getCamera().combined);
        hudShapeRenderer = new ShapeRenderer();
        hudShapeRenderer.setAutoShapeType(true);
        hudShapeRenderer.setProjectionMatrix(hudViewport.getCamera().combined);
        hudCamera.update();
    }

    @Override
    public void render() {
        handelCameraUpdates();
        var tileAtMouse = getTileAtMousePosition();
        drawMap();
        drawHud(tileAtMouse);

        if (Gdx.input.isKeyJustPressed(Input.Keys.E) && !state().isGameOver()) {
            state().nextRound();
        }

    }

    private void drawHud(TileCoord tileAtMouse) {
        hudShapeRenderer.begin();
        hudShapeRenderer.setColor(Color.GRAY);
        hudShapeRenderer.set(ShapeRenderer.ShapeType.Filled);
        hudShapeRenderer.rect(0, GameRenderer.V_HEIGHT - 50, GameRenderer.V_WIDTH, 50);
        hudShapeRenderer.rect(0, 0, GameRenderer.V_WIDTH, 100);
        hudShapeRenderer.end();

        updateMapCamera();


        hudBatch.begin();
        font().draw(hudBatch, "Food: %s (%s)".formatted(state().getFood(), state().calculateFoodYield() - state().getProjectedFoodConsumption()), 10, GameRenderer.V_HEIGHT - 13);
        font().draw(hudBatch, "Pop: %s (%s)".formatted(state().getEntity().getPopulation(), state().getEntity().calculatePopDevelopment()), 200, GameRenderer.V_HEIGHT - 13);

        font().draw(hudBatch, "Round: %s".formatted(state().getRound()), GameRenderer.V_WIDTH - 200, GameRenderer.V_HEIGHT - 13);

        if (state().getEntity().getPopulation() < 1) {
            font().draw(hudBatch, "DUE TO YOUR UTTER INCOMPETENCE THE WHOLE TRIBE PERISHED!", GameRenderer.V_WIDTH / 2, GameRenderer.V_HEIGHT / 2);
            if (state().getEarlyTribeTile().isPresent()) {
                state().setGameOver(true);
                state().getEarlyTribeTile().ifPresent(tile -> tile.tile().setEntity(null));
            }
        }

        font().draw(hudBatch, renderReslectedTileString(), 0, 20);
        font().draw(hudBatch, "Current Yield: %s".formatted(state().calculateFoodYield()), 0, 40);


        hudBatch.end();
    }

    private String renderReslectedTileString() {
        if (selected == null) {
            return "-";
        }
        return "Selected-Tile: %s/%s".formatted(selected.getResources(), Tile.HUNTER_GATHERER_RESOURCE_CAP);
    }

    private void drawMap() {
        mapBatch.begin();
        for (int y = 0; y < state().getHeight(); y++) {
            for (int x = 0; x < state().getWidth(); x++) {
                Tile tile = state().getTiles()[y][x];
                var xCoord = x * Tile.NORMAL_TILE_SIZE;
                var yCoord = y * Tile.NORMAL_TILE_SIZE;

                mapBatch.draw(tile.getTerrain().getTexture(), xCoord, yCoord);
                if (tile.getDecorator() != null) {
                    mapBatch.draw(tile.getDecorator().getTexture(), xCoord, yCoord);
                }
                if (tile.getEntity() != null) {
                    mapBatch.draw(tile.getEntity().getTexture(), xCoord, yCoord);
                }

                if(mapCamera.zoom <= 2) {
                    font().draw(mapBatch, "%s/%s".formatted(tile.getResources(), Tile.HUNTER_GATHERER_RESOURCE_CAP), xCoord + 25, yCoord + 25);
                }


            }
        }
        mapBatch.end();


        mapShapeRenderer.begin();
        mapShapeRenderer.set(ShapeRenderer.ShapeType.Line);

        for (int y = 0; y < state().getHeight(); y++) {
            for (int x = 0; x < state().getWidth(); x++) {
                var xCoord = x * Tile.NORMAL_TILE_SIZE;
                var yCoord = y * Tile.NORMAL_TILE_SIZE;
                Tile tile = state().getTiles()[y][x];

                if (tile == selected) {
                    mapShapeRenderer.setColor(Color.RED);
                } else {
                    mapShapeRenderer.setColor(Color.LIGHT_GRAY);
                }

                mapShapeRenderer.rect(xCoord, yCoord, Tile.NORMAL_TILE_SIZE, Tile.NORMAL_TILE_SIZE);
            }
        }
        mapShapeRenderer.end();
    }

    private BitmapFont font() {
        return Fonts.getInstance().getNormalFont();
    }

    public void handleClick() {
        var tileAtMouse = getTileAtMousePosition();
        if (selected != null) {
            Entity entity = selected.getEntity();
            if (selected.getEntity() != null && entity.canMove(tileAtMouse) && selected != tileAtMouse.tile()) {
                tileAtMouse.tile().setEntity(entity);
                entity.onMove();
                selected.setEntity(null);
                selected = null;
            } else {
                selected = tileAtMouse.tile();
            }
        } else {
            selected = tileAtMouse.tile();
        }
    }


    private TileCoord getTileAtMousePosition() {
        var unProjected = mapCamera.unproject(new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0));
        int i = ((int) unProjected.y - ((int) unProjected.y % Tile.NORMAL_TILE_SIZE)) / Tile.NORMAL_TILE_SIZE;
        int j = ((int) unProjected.x - ((int) unProjected.x % Tile.NORMAL_TILE_SIZE)) / Tile.NORMAL_TILE_SIZE;


        if (i >= state().getTiles().length || i < 0) {
            i = 0;
        }

        if (j >= state().getTiles()[i].length || j < 0) {
            j = 0;
        }

        return new TileCoord(state().getTiles()[i][j], new Index2D(i, j));

    }

    private void handelCameraUpdates() {

        if (Gdx.input.isKeyPressed(Input.Keys.NUMPAD_ADD)) {
            updateZoom(-this.mapCamera.zoom * Gdx.graphics.getDeltaTime());
        }

        if (Gdx.input.isKeyPressed(Input.Keys.NUMPAD_SUBTRACT)) {
            updateZoom(this.mapCamera.zoom * Gdx.graphics.getDeltaTime());
        }


        Map<Integer, Vector2> translationMap = Map.of(
                Input.Keys.LEFT, new Vector2(-50, 0),
                Input.Keys.RIGHT, new Vector2(50, 0),
                Input.Keys.UP, new Vector2(0, 50),
                Input.Keys.DOWN, new Vector2(0, -50)
        );

        for (var entry : translationMap.entrySet()) {
            if (Gdx.input.isKeyPressed(entry.getKey())) {
                mapCamera.translate(entry.getValue().x * this.mapCamera.zoom, entry.getValue().y * this.mapCamera.zoom);
                updateMapCamera();
            }
        }


    }

    private void updateZoom(float baseline) {
        mapCamera.zoom += (baseline * 1.f);

        if (mapCamera.zoom < 0.1f) {
            mapCamera.zoom = 0.1f;
        }

        if (mapCamera.zoom > 5) {
            mapCamera.zoom = 5;
        }

        updateMapCamera();
    }

    private void updateMapCamera() {
        mapCamera.update();
        mapBatch.setProjectionMatrix(mapCamera.combined);
        mapShapeRenderer.setProjectionMatrix(mapCamera.combined);
    }

    private GameState state() {
        return GameState.getInstance();
    }

    @Override
    public void resize(int width, int height) {
        mapViewPort.update(width, height);
        hudViewport.update(width, height);
    }

}
