package de.sollder1.civklon.views;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.ApplicationListener;

public interface View extends ApplicationListener {

    default void create() {
    }

    default void resize(int width, int height) {
    }

    default void render() {
    }

    default void pause() {
    }


    default void resume() {
    }


    default void dispose() {
    }

    void handleClick();

}
