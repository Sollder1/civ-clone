package de.sollder1.civklon.views;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import de.sollder1.civklon.GameRenderer;
import de.sollder1.civklon.constant.Fonts;
import de.sollder1.civklon.state.GameState;
import de.sollder1.civklon.constant.GlobalConstants;
import de.sollder1.civklon.i18n.Translator;
import de.sollder1.civklon.technology.TechGraph;

import java.util.List;
import java.util.stream.Collectors;

public class ResearchView implements View {

    public static final int ITEM_HEIGHT = 200;
    public static final int ITEM_WIDTH = 400;
    public static final int LEFT_OFFSET = 50;
    public static final int BOTTOM_OFFSET = 100;


    private Viewport hudViewport;
    private SpriteBatch hudBatch;
    private ShapeRenderer hudShapeRenderer;

    public ResearchView() {

        OrthographicCamera hudCamera = new OrthographicCamera();
        hudCamera.setToOrtho(false, GameRenderer.V_WIDTH, GameRenderer.V_HEIGHT);
        hudViewport = new FitViewport(GameRenderer.V_WIDTH, GameRenderer.V_HEIGHT, hudCamera);
        hudViewport.apply();
        hudViewport.update(GameRenderer.V_WIDTH, GameRenderer.V_HEIGHT);
        hudBatch = new SpriteBatch();
        hudBatch.setProjectionMatrix(hudViewport.getCamera().combined);
        hudShapeRenderer = new ShapeRenderer();
        hudShapeRenderer.setAutoShapeType(true);
        hudShapeRenderer.setProjectionMatrix(hudViewport.getCamera().combined);
        hudCamera.update();
    }

    @Override
    public void render() {
        drawHud();
    }

    private void drawHud() {
        hudShapeRenderer.begin();
        hudShapeRenderer.setColor(Color.GRAY);
        hudShapeRenderer.set(ShapeRenderer.ShapeType.Filled);
        hudShapeRenderer.rect(0, 0, GameRenderer.V_WIDTH, GameRenderer.V_HEIGHT);
        hudShapeRenderer.end();

        hudBatch.begin();


        int slotCounter = 0;
        for (var phase : GlobalConstants.TECH_GRAPH.getPhases()) {
            for (List<TechGraph.Node> nodesOfSlot : phase.techs()) {
                int x = (slotCounter * ITEM_WIDTH) + LEFT_OFFSET;
                for (int i = 0; i < nodesOfSlot.size(); i++) {
                    TechGraph.Node node = nodesOfSlot.get(i);
                    setFontColor(node);

                    int y = (i * ITEM_HEIGHT) + ITEM_HEIGHT + BOTTOM_OFFSET;
                    font().draw(hudBatch, "%s".formatted(translate(node.id())), x, y);
                    y -= 20 + 8;

                    if (!node.dependsOn().isEmpty()) {
                        String depends = node.dependsOn().stream()
                                .map(GlobalConstants.TECH_GRAPH::byId)
                                .map(dep -> translate(dep.id()))
                                .collect(Collectors.joining(", "));
                        getSmallFont().draw(hudBatch, "%s: %s".formatted(translate("requires"), depends), x, y);
                    }
                    y -= 20;


                    GlyphLayout layout = new GlyphLayout(getSmallFont(),
                            "%s".formatted(translate(node.id() + "_desc")), Color.WHITE, ITEM_WIDTH, Align.left, true);

                    getSmallFont().draw(hudBatch, layout, x, y);
                }
                slotCounter++;
            }
            font().setColor(Color.WHITE);
        }

        if(getGameState().getCurrentResearch() == null) {
            font().draw(hudBatch, "%s".formatted(translate("selectResearch")), LEFT_OFFSET, 25);
        } else {
            font().draw(hudBatch, "%s: %s / %s".formatted(translate("progress"), getGameState().getCurrentResearchProgress(),
                    getGameState().getCurrentResearch().researchRequired()), LEFT_OFFSET, 25);
        }


        hudBatch.end();
    }

    private static BitmapFont getSmallFont() {
        return Fonts.getInstance().getSmallFont();
    }

    private static GameState getGameState() {
        return GameState.getInstance();
    }

    private void setFontColor(TechGraph.Node node) {
        if (getGameState().getCurrentTechnologies().contains(node.id())) {
            font().setColor(Color.GREEN);
        } else if (getGameState().getCurrentResearch() == node) {
            font().setColor(Color.ORANGE);
        } else {
            font().setColor(Color.WHITE);
        }
    }

    @Override
    public void handleClick() {

        var unproject = hudViewport.unproject(new Vector2(Gdx.input.getX(), Gdx.input.getY()));

        int y = (int) unproject.y - BOTTOM_OFFSET;
        int i = (y - y % ITEM_HEIGHT) / ITEM_HEIGHT;

        int x = (int) unproject.x - LEFT_OFFSET;
        int j = (x - x % ITEM_WIDTH) / ITEM_WIDTH;

        var flattenedSlots = GlobalConstants.TECH_GRAPH.getFlattenedSlots();
        if (flattenedSlots.size() > j && flattenedSlots.get(j).size() > i) {
            TechGraph.Node node = flattenedSlots.get(j).get(i);
            if (canResearchNow(node)) {
                getGameState().setCurrentResearch(node);
            }
        }
    }

    private static boolean canResearchNow(TechGraph.Node node) {
        return !getGameState().getCurrentTechnologies().contains(node.id())
                && getGameState().getCurrentTechnologies().containsAll(node.dependsOn());
    }


    private String translate(String key) {
        return Translator.translateTech(key);
    }


    private BitmapFont font() {
        return Fonts.getInstance().getNormalFont();
    }

    @Override
    public void resize(int width, int height) {
        hudViewport.update(width, height);
    }

}
