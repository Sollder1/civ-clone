package de.sollder1.civklon.tiles;

import com.badlogic.gdx.files.FileHandle;
import de.sollder1.civklon.constant.GlobalConstants;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;

public final class BmpTileLoader {

    public static Tile[][] fromBmp(FileHandle fileHandle) {
        try {
            BufferedImage read = ImageIO.read(fileHandle.read());
            Tile[][] tiles = new Tile[read.getHeight()][read.getWidth()];
            for (int y = 0; y < read.getHeight(); y++) {
                for (int x = 0; x < read.getWidth(); x++) {
                    String hex = readColorAtPos(read, x, y);
                    TileTerrain tileTerrain = GlobalConstants.TILE_TERRAINS.stream()
                            .filter(tt -> tt.getMeta().mapColor().equals(hex))
                            .findFirst().orElseThrow();
                    tiles[y][x] = new Tile(tileTerrain);
                }
            }
            return tiles;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }


    private static String readColorAtPos(BufferedImage read, int x, int y) {
        int rgb = read.getRGB(x, y);
        int blue = rgb & 0xff;
        int green = (rgb & 0xff00) >> 8;
        int red = (rgb & 0xff0000) >> 16;
        return String.format("#%02x%02x%02x", red, green, blue).toUpperCase();
    }
}

