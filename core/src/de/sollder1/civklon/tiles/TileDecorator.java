package de.sollder1.civklon.tiles;

import com.badlogic.gdx.graphics.Texture;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString
public class TileDecorator {

    private final JsonModel baseFromJson;
    private final Texture texture;

    public TileDecorator(JsonModel model) {
        baseFromJson = model;
        texture = new Texture(model.model());
    }

    public record JsonModel(String id, String model, List<String> applicableTo) {

    }

}
