package de.sollder1.civklon.tiles;

import de.sollder1.civklon.EndTurnAware;
import de.sollder1.civklon.state.GameState;
import de.sollder1.civklon.entities.Entity;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Tile implements EndTurnAware {

    public static final int NORMAL_TILE_SIZE = 100;
    public static final int HUNTER_GATHERER_RESOURCE_CAP = 6;


    private final TileTerrain terrain;
    private TileDecorator decorator;
    private Entity entity;

    private int resources = HUNTER_GATHERER_RESOURCE_CAP;
    private int lastHarvest;

    public Tile(TileTerrain terrain) {
        this.terrain = terrain;
    }

    public String getTerrainId() {
        return terrain.getMeta().id();
    }

    public void endTurn() {
        int currentRound = GameState.getInstance().getRound();
        if (lastHarvest != currentRound && resources < HUNTER_GATHERER_RESOURCE_CAP) {
            if ((currentRound - lastHarvest) % 3 == 0) {
                resources++;
            }
        }
    }

    public int harvest(int maxFoodYield) {
        lastHarvest = GameState.getInstance().getRound();

        if (resources < maxFoodYield) {
            maxFoodYield = resources;
            resources = 0;
            return maxFoodYield;
        }

        resources -= maxFoodYield;
        return maxFoodYield;
    }

}
