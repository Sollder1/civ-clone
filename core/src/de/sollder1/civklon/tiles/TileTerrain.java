package de.sollder1.civklon.tiles;

import com.badlogic.gdx.graphics.Texture;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString

public class TileTerrain {

    public static final String GRASS_LAND_ID = "grassland";
    public static final String SHALLOW_WATER_ID = "shallow_water";


    private final JsonModel meta;
    private final Texture texture;

    public TileTerrain(JsonModel model) {
        meta = model;
        texture = new Texture(model.background());
    }

    public record JsonModel(String id, String background, String mapColor, boolean passable) {

    }

}
