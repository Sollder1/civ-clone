package de.sollder1.civklon.i18n;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Json;
import com.fasterxml.jackson.core.type.TypeReference;
import de.sollder1.civklon.utils.JsonUtils;

import java.util.Locale;
import java.util.Map;

public final class Translator {


    private Translator() {
    }


    public static final String TECH_CONTEXT = "tech";

    private static Map<String, Map<String, String>> TRANSLATOR_CACHE;


    public static void loadLanguage(Locale locale) {
        FileHandle yml = Gdx.files.internal("translations/%s.yml".formatted(locale.getLanguage()));
        TypeReference<Map<String, Map<String, String>>> reference = new TypeReference<>() {
        };

        TRANSLATOR_CACHE = JsonUtils.loadYml(reference, yml.read());
    }


    public static String translate(String context, String key) {

        if (!TRANSLATOR_CACHE.containsKey(context)) {
            return context + "-" + key;
        }

        Map<String, String> translations = TRANSLATOR_CACHE.get(context);

        if (!translations.containsKey(key)) {
            return context + "-" + key;
        }

        return translations.get(key);
    }

    public static String translateTech(String key) {
        return translate(TECH_CONTEXT, key);
    }
}
