package de.sollder1.civklon.technology;

public interface Event {
    void trigger();
}
