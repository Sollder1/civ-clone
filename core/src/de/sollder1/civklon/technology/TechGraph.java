package de.sollder1.civklon.technology;

import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.annotation.Nulls;
import de.sollder1.civklon.state.GameState;
import lombok.Getter;
import lombok.Setter;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Getter
@Setter
public class TechGraph {

    public static final String EARLY_SEA_FARE = "early_sea_fare";
    private List<TechGraph.Phase> phases;

    private Map<String, Node> nodeIdLookup;
    private Map<String, Phase> phaseIdLookup;
    private List<List<Node>> flattenedSlots;


    public TechGraph(List<TechGraph.Phase> phases) {
        this.phases = phases;
        phaseIdLookup = phases.stream()
                .collect(Collectors.toMap(TechGraph.Phase::id, Function.identity()));
        nodeIdLookup = phases.stream()
                .flatMap(phase -> phase.techs().stream())
                .flatMap(Collection::stream)
                .collect(Collectors.toMap(Node::id, Function.identity()));
        flattenedSlots = phases.stream()
                .flatMap(phase -> phase.techs().stream())
                .toList();
    }

    public int getFoodYield(String terrainId) {
        return nodeIdLookup.values().stream()
                .filter(node -> GameState.getInstance().getCurrentTechnologies().contains(node.id()))
                .filter(node -> node.terrainMods != null)
                .filter(node -> node.terrainMods.containsKey(terrainId))
                .mapToInt(node -> node.terrainMods.get(terrainId).food())
                .sum();
    }

    public Node byId(String id) {
        return nodeIdLookup.get(id);
    }

    public Set<String> getPassableTerrains() {
        return GameState.getInstance().getCurrentTechnologies().stream()
                .map(id -> nodeIdLookup.get(id))
                .flatMap(node -> node.passable().stream())
                .collect(Collectors.toSet());

    }


    public record Phase(String id, int from, int to, @JsonSetter(nulls = Nulls.AS_EMPTY) List<List<Node>> techs) {

    }

    public record Node(String id,
                       String phase,
                       float researchRequired,
                       @JsonSetter(nulls = Nulls.AS_EMPTY) Map<String, TerrainMod> terrainMods,
                       String triggers,
                       @JsonSetter(nulls = Nulls.AS_EMPTY) Set<String> passable,
                       @JsonSetter(nulls = Nulls.AS_EMPTY) List<String> dependsOn) {

    }


    public record TerrainMod(int food) {

    }

}
