package de.sollder1.civklon.technology.stoneage;

import de.sollder1.civklon.state.GameState;
import de.sollder1.civklon.technology.Event;

public class EarlySpiritualityEvent implements Event {

    @Override
    public void trigger() {
        GameState.getInstance().setTechnologyYieldPerPop(0.2f);
    }

}
