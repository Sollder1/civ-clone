package de.sollder1.civklon;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Graphics;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.utils.ScreenUtils;
import de.sollder1.civklon.constant.Fonts;
import de.sollder1.civklon.i18n.Translator;
import de.sollder1.civklon.state.GameState;
import de.sollder1.civklon.tiles.BmpTileLoader;
import de.sollder1.civklon.views.MapView;
import de.sollder1.civklon.views.ResearchView;
import de.sollder1.civklon.views.View;

import java.util.Locale;

public class GameRenderer extends ApplicationAdapter {

    public static int V_WIDTH = 1920;
    public static int V_HEIGHT = 1080;

    private MapView mapView;
    private ResearchView researchView;
    private View currentView;
    private Music music;
    private boolean lastTouched = false;

    public BitmapFont textFont;

    @Override
    public void create() {
        Translator.loadLanguage(Locale.GERMAN);
        music = Gdx.audio.newMusic(Gdx.files.internal("music/music_01.mp3"));
        music.setLooping(true);
        music.play();

        Fonts.init();

        initGameState();

        mapView = new MapView();
        researchView = new ResearchView();
        currentView = mapView;
    }


    @Override
    public void render() {

        ScreenUtils.clear(Color.BLACK);
        handleClick();
        handleScreenModeToggle();
        handleMusicControl();
        handleScreenControl();

        if (Gdx.input.isKeyJustPressed(Input.Keys.R)) {
            initGameState();
        }

        currentView.render();
    }

    private void handleClick() {
        boolean isTouched = Gdx.input.isTouched();
        if (lastTouched && !isTouched) {
            currentView.handleClick();
        }
        lastTouched = isTouched;
    }


    private void handleScreenControl() {

        if (Gdx.input.isKeyJustPressed(Input.Keys.F1)) {
            currentView = mapView;
        }

        if (Gdx.input.isKeyJustPressed(Input.Keys.F2)) {
            currentView = researchView;
        }

    }

    private void handleMusicControl() {
        if (Gdx.input.isKeyJustPressed(Input.Keys.M)) {
            if (music.isPlaying()) {
                music.pause();
            } else {
                music.play();
            }
        }
    }

    private static void handleScreenModeToggle() {
        if (Gdx.input.isKeyJustPressed(Input.Keys.F)) {
            var fullScreen = Gdx.graphics.isFullscreen();
            Graphics.DisplayMode currentMode = Gdx.graphics.getDisplayMode();
            if (fullScreen) {
                Gdx.graphics.setWindowedMode(currentMode.width, currentMode.height);
            } else {
                Gdx.graphics.setFullscreenMode(currentMode);
            }
        }
    }

    private static void initGameState() {
        GameState.init(BmpTileLoader.fromBmp(Gdx.files.internal("maps/basic_map.bmp")));
    }


    @Override
    public void dispose() {
        currentView.dispose();
        music.dispose();
        Fonts.cleanup();
    }

    @Override
    public void resize(int width, int height) {
        currentView.resize(width, height);
    }
}
