package de.sollder1.civklon;

public interface EndTurnAware {
    void endTurn();
}
